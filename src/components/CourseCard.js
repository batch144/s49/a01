import {useState, useEffect} from 'react';
import {Container, Row, Col, Button, Card} from 'react-bootstrap'


export default function CourseCard({courseProp}){

  console.log(courseProp)

  /*object destructuring*/

  const {name, description, price, id, onOffer} = courseProp

  // console.log(name)
  // console.log(description)
  // console.log(price)
  // console.log(id)
  // console.log(onOffer)


  /*array destructuring*/
  // const arr = ["one", "two", "three", "four"]

  // const [elem1, elem2, , elem4] = arr
  // console.log(elem1)
  // console.log(elem2)
  // console.log(elem4)

  /*useState hook*/
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(10);

  const [isDisabled, setIsDisabled] = useState(false);

  function enroll(){
    setCount(count + 1)
    setSeats(seats - 1)
  }

  useEffect( () => {
    if(seats === 0){
      setIsDisabled(true)
    }

  }, [seats] )

	return(

		<Container fluid className="m-3">
      <Row>
        <Col>
          <Card>
            <Card.Body>
              <Card.Title>{name}</Card.Title>
                <Card.Subtitle>
                Description:
                </Card.Subtitle>
                  <Card.Text>
                    {description}
                  </Card.Text>
                <Card.Subtitle>
                 Price:
                </Card.Subtitle>
                  <Card.Text>
                    {price}
                  </Card.Text>
              <Card.Text>Enrollees: {count}</Card.Text>
              <Card.Text>Seats: {seats}</Card.Text>
              <Button variant="primary" onClick={enroll} disabled={isDisabled}>Enroll</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>

		)
}