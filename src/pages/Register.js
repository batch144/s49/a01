import { useState, useEffect, useContext, Fragment } from 'react';
import { Container, Form, Button } from 'react-bootstrap';

import UserContext from './../UserContext'
import Course from './Course'

export default function Register(){

	const { user } = useContext(UserContext);

  let reg = (user.email != null) ? 
    <Fragment>
      <Course />
    </Fragment>
  : 
    <Fragment>
      <Register />
    </Fragment>

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")

	const [isDisabled, setIsDisabled] = useState();

	useEffect( () => {
    	if(email !== "" && password !== "" && cpw !== "" && password === cpw){
      		setIsDisabled(false)
    	} else {
    		setIsDisabled(true)
    	}
  	}, [email, password, cpw] )

  	function Register(e){
  		e.preventDefault()

  		alert("Registered Successfully!")

  		setEmail("")
  		setPassword("")
  		setCpw("")
  	}

	return(
		
		<Container fluid className="m-3">
		<h1>Register</h1>
			<Form className="border p-3 my-3" onSubmit={ (e) => Register(e) }>
		{/*email*/}
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email}
			    	onChange={ (e) => setEmail(e.target.value) } />
			  </Form.Group>
		{/*password*/}
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password}
			    	onChange={ (e) => setPassword(e.target.value) } />
			  </Form.Group>
		{/*confirm password*/}
				<Form.Group className="mb-3" controlId="cpw">
				  <Form.Label>Verify Password</Form.Label>
				  <Form.Control 
				  	type="password" 
				  	placeholder="Verify Password" 
				  	value={cpw}
				  	onChange={ (e) => setCpw(e.target.value) } />
				</Form.Group>
			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}