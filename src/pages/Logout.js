import { Redirect } from 'react-router-dom';
import { useContext, useEffect } from 'react'

import UserContext from './../UserContext'

export default function Logout(){
	const { unsetUser, setUser } = useContext(UserContext);

	//Clear localStorage of the user's info
	unsetUser();

	useEffect(() => {
		//set user state to original value
		setUser({email: null})
	}, [])

	return(

		<Redirect to="/login"/>
	)
}