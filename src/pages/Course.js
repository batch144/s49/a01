
import { Fragment } from 'react';

import courseData from './../data/courseData';

/*components*/
	/*course card is the template for course*/
import CourseCard from './../components/CourseCard';

export default function Course(){

	console.log(courseData)

	const course = courseData.map( element => {
		return (
			<CourseCard key={element.id} courseProp={element}/>
		)
	})

	return(
		<Fragment>
			{course}
		</Fragment>

	)
}