import {Fragment} from 'react'

import 'bootstrap/dist/css/bootstrap.min.css';

import Banner from './../components/Banner';
import Highlights from './../components/Highlights';


export default function Home(){
	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/course",
		label: "Enroll now!"
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights/>
		</Fragment>

		)
}