
import { Fragment, useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

/*for React Context*/
import { UserProvider } from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Course from './pages/Course';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';

export default function App() {

  const [user, setUser] = useState({

    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear();
  }



  return(
    <UserProvider value={ {user, setUser, unsetUser} }>
      <BrowserRouter>
        <AppNavbar/>
        <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/course" component={Course}/>
              <Route exact path="/register" component={Register}/>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/logout" component={Logout}/>
              <Route exact path="*" component={NotFound} />
        </Switch>
      </BrowserRouter>
    </UserProvider>
  )
}

