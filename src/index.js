import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/*const name = "Elito"
const greeting = <h1>Happy new year, {name}!</h1>*/

ReactDOM.render(
  <React.StrictMode>
    <App />
    {/*<h1>Hello</h1>
    {greeting}*/}
  </React.StrictMode>,
  document.getElementById('root')
);